package cn.itlaobing.springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.itlaobing.springboot.entity.User;
import cn.itlaobing.springboot.repository.UserRepository;

@Controller
public class UserController {
	
	@Autowired
	UserRepository userRepository;
	
	@GetMapping("/hello")
	public String hello(User user) {
		
		userRepository.save(user);
		return "Hello springboo!";
	}
	
	@GetMapping("/list")
	public String list(Model model) {
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		model.addAttribute("userList", userRepository.findAll());
		return "userlist";
	}
	
	@GetMapping("/index")
	public String index() {
		return "index";
	}
	
	

}
